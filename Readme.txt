Aplicatie dispozitive mobile  - Biblioteca personala

- Aplicatia va permite gestiunea unei colectii personale de carti.
- Utilizatorul va putea vedea cartile din biblioteca personala
- De asemenea acesta va putea adauga, sterge sau edita informatii referitoare la o carte
- O carte se poate salva sub urmatoarele statusuri: to read, red si currently reading
- Pentru fiecare carte, indiferent de status se va salva titlul, autorul, editura, anul aparitiei, scurta descriere

